import React, { useContext } from 'react';
import { View, Text, FlatList } from 'react-native';
import { RootContext } from './Soal2'

const DataList = () => {

    const state = useContext(RootContext)

    const renderItem = ({ item, index }) => {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Text>{item.name}</Text>
                <Text>{item.position}</Text>
            </View>
        )
    }

    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            {/* <Text>TES</Text> */}
            <FlatList
                data={state.name}
                renderItem={renderItem}
                // CATATAN : Cara mengambil nilai index yang tidak terdefinisikan id/key nya (Sumber live session 2)
                keyExtractor={(item, index) => index.toString()}
                ItemSeparatorComponent={() => <View style={{ height: 20 }} />}
            />
        </View>
    )

}

export default DataList;