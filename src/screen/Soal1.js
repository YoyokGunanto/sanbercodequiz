import React, { Component, useState, useEffect } from 'react'
import { Text, View } from 'react-native'
// CATATAN : Sumber Youtube Prawito Hudoro

const FComponent = () => {

    const [name, setName] = useState('Jhon Doe');

    useEffect(() => {
        setTimeout(() => {
            setName('Asep');
        }, 3000);
    }, [name]);

    // constructor(props) {
    //     super(props);
    //     this.state = {
    //         name: 'Jhon Doe'
    //     }
    // }

    // componentDidMount() {
    //     setTimeout(() => {
    //         this.setState({
    //             name: 'Asep'
    //         })
    //     }, 3000)
    // }

    // render() {
    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Text>{name}</Text>
            {/* <Text>{this.state.name}</Text> */}
        </View>
    )
    // }
}

export default FComponent;